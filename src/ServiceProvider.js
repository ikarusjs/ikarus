module.exports = class ServiceProvider {

    constructor({ app }) {
        this.app = app
    }

    async register() {

    }

    async boot() {

    }

    static serviceName() {
        return this.name.slice(0, this.name.indexOf('ServiceProvider')).toLowerCase()
    }

    serviceName() {
        return this.constructor.serviceName()
    }

    configKey() {
        return this.serviceName()
    }

    get config() {
        return this.app.config[this.configKey()]
    }

    static config(config) {
        // Has to be ugly, otherwise class has name of assigned variable name
        return ({ [this.name]: class extends this {
            get config() {
                return {
                    ...super.config,
                    ...config,
                }
            }
        } }) [this.name]
    }
}