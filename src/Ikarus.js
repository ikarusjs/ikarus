module.exports = class Ikarus {

    constructor(providers) {
        this.services = {}
        for(const provider of providers) {
            this.services[provider.serviceName()] = new provider({ app: this })
        }
    }

    static providers() {
        return []
    }

    async boot() {
        for(const service in this.services) {
            await this.services[service].register()
        }

        for(const service in this.services) {
            await this.services[service].boot()
        }
    }

    static async create() {
        const instance = new this(this.providers())
    
        await instance.boot()
    
        return instance
      }

}