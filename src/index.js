module.exports = {
    ...require('./mix'),
    Ikarus: require('./Ikarus'),
    ServiceProvider: require('./ServiceProvider'),
}