import ServiceProvider from "./ServiceProvider";
export default class Ikarus {
    protected providers: ServiceProvider[];
    constructor(providers: (typeof ServiceProvider)[]);
    static providers(): (typeof ServiceProvider[]);
    boot(): Promise<void>;
    static create(): Promise<Ikarus>;
}
