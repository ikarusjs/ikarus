import ServiceProvider from "../ServiceProvider";
export default class HttpServiceProvider extends ServiceProvider {
    register(): Promise<void>;
    boot(): Promise<void>;
}
