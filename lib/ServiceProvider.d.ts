import Ikarus from "./Ikarus";
export default class ServiceProvider {
    protected app: Ikarus & {
        [key: string]: any;
    };
    constructor({ app }: {
        app: Ikarus;
    });
    register(): Promise<any>;
    boot(): Promise<any>;
}
