const supertest = require('supertest')
const Cat = require('./app/src/containers/cats/model')

const dummy_cat = { name: 'Basti', color: 'gray' }

test('uses .env file correctly', async () => {
    const app = await require('./app/src').create()

    expect(app.config).toHaveProperty('important_port')
    expect(app.config).toHaveProperty('important_key')
    expect(app.config.important_port).toBe(1234)
    expect(app.config.important_key).toBe('important-key')
})


test('Example App', async () => {
    const app = await require('./app/src').create()
    const request = supertest(app.http)

    await request.post('/cats').send(dummy_cat).then(response => {
        expect(response.body).toMatchObject(dummy_cat)
        expect(response.body).toHaveProperty('_id')

        dummy_cat._id = response.body._id
    })

    await request.patch('/cats/' + dummy_cat._id).send({ owner: 'me' }).then(response => {
        expect(response.body).toMatchObject(dummy_cat)
        expect(response.body.owner).toBe('me')
    })

    await request.put('/cats/' + dummy_cat._id).send(dummy_cat).then(response => {
        expect(response.body).toMatchObject(dummy_cat)
        expect(response.body.owner).toBeUndefined()
    })

    await request.get('/cats/' + dummy_cat._id).then(response => {
        expect(response.body).toMatchObject(dummy_cat)
    })
    
    await request.get('/cats').then(response => {
        expect(response.body).toMatchObject([ dummy_cat ])
    })
    
})