const Ikarus = require('../src/Ikarus')

module.exports = async ({ providers = [] } = {}) => {
    return await (class App extends Ikarus { static providers() { return providers } }).create()
}