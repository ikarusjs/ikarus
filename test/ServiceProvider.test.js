const { ServiceProvider } = require('../src')

test('sets app property', () => {
    const instance = new ServiceProvider({ app: 'your app' })
    expect(instance.app).toBe('your app')
})

test('automatic config key', () => {
    class CustomServiceProvider extends ServiceProvider {}
    const instance = new CustomServiceProvider({ app: { config: { custom: 'works' } } })

    expect(instance.configKey()).toBe('custom')
    expect(instance.config).toBe('works')
})

test('static config', () => {
    class CustomServiceProvider extends ServiceProvider {}
    const instance = new (CustomServiceProvider.config({ b: false, c: 'new value' }))({ app: { config: { custom: { a: 'yes', b: true } } } })

    expect(instance.config.a).toBe('yes')
    expect(instance.config.b).toBe(false)
    expect(instance.config.c).toBe('new value')
})