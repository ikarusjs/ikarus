const HttpInterface = require('../../../../../src/Http/Interfaces/HttpInterface')
const hasRestModel = require('../../../../../src/Http/Interfaces/hasRestModel')
const { mix } = require('../../../../../src/mix')

class CatHttpInterface extends mix(HttpInterface).with(hasRestModel) {
    model() {
        return require('./model')
    }
}

module.exports = {
    CatHttpInterface
}