const { CatHttpInterface } = require('./interfaces')

module.exports = class CatContainer {

    model() {
        return require('./model')
    }

    interfaces() {
        return {
            http: CatHttpInterface
        }
    }

}