const NedbModel = require('../../../../../src/Nedb/NedbModel')

module.exports = class Cat extends NedbModel {

    constructor({ color, name }) {
        this.id = null
        this.color = color
        this.name = name
    }

    static primaryKey() {
        return '_id'
    }

}