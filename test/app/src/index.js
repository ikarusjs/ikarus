const Ikarus = require('../../../src/Ikarus')
const CatContainer = require('./containers/cats')

module.exports = class App extends Ikarus {

    static providers() {
        return require('./providers')
    }

    static containers() {
        return {
            '/cats': new CatContainer()
        }
    }

    get config() {
        return require('../config')
    }

}