const ContainerServiceProvider = require('../../../../src/ServiceProviders/ContainerServiceProvider')
const HttpServiceProvider = require('../../../../src/Http/HttpServiceProvider')
const NedbServiceProvider = require('../../../../src/Nedb/NedbServiceProvider')
const ConfigServiceProvider = require('../../../../src/Config/ConfigServiceProvider')

module.exports = [
    ConfigServiceProvider.config({ path: './test/app/.env' }),
    ContainerServiceProvider,
    HttpServiceProvider,
    NedbServiceProvider,
]