module.exports = {
    important_key: process.env.IMPORTANT_KEY,
    important_port: parseInt(process.env.IMPORTANT_PORT),

    http: require('./http'),
}