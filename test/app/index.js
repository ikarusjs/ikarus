const App = require('./src')

App.create().then(app => {
    app.http.listen(app.config.http.port || 3030)
})