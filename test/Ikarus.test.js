const Ikarus = require('../src/Ikarus')
const ServiceProvider = require('../src/ServiceProvider')

class TestServiceProvider extends ServiceProvider {
    async register() {
        this.app.register_works = true
        this.property = 'value'
    }
    async boot() {
        this.app.boot_works = true
    }
}

class App extends Ikarus {
    static providers() {
        return [
            TestServiceProvider
        ]
    }
}

describe('Ikarus App', () => {
    
    test('Is App instance', async () => {
        const app = await App.create()
        expect(app).toBeInstanceOf(App)
    })
    
    test('Service Provider works', async () => {
        const app = await App.create()
        expect(app.register_works).toBe(true)
        expect(app.boot_works).toBe(true)
    })

    test('Service Provider can be accessed', async () => {
        const app = await App.create()
        expect(app.services.test).toBeInstanceOf(TestServiceProvider)
        expect(app.services.test.property).toBe('value')
    })

})